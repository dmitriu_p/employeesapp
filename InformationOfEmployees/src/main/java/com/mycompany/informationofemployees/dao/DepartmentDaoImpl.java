/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.informationofemployees.dao;

import com.mycompany.informationofemployees.model.Department;
import java.util.List;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Дмитрий
 */

@Repository("departmentDao")
public class DepartmentDaoImpl extends AbstractDao<Integer, Department>implements DepartmentDao{
    
    @Override
    public Department findById(int id) {
        return getByKey(id);
    }
 
    @Override
    public Department findByType(String name) {
        try{
            Department department = (Department) getEntityManager()
                    .createQuery("SELECT p FROM Department p WHERE p.name LIKE :name")
                    .setParameter("name", name)
                    .getSingleResult();
            return department; 
        }catch(NoResultException ex){
            System.out.println("ex: "+ex);
            return null;
        }
    }
     
    @SuppressWarnings("unchecked")
    @Override
    public List<Department> findAll(){
        List<Department> department = getEntityManager()
                .createQuery("SELECT p FROM Department p  ORDER BY p.name ASC")
                .getResultList();
        return department;
    }
}
