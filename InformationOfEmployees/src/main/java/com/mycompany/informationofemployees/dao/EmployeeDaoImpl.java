/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.informationofemployees.dao;

import com.mycompany.informationofemployees.model.Employee;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Дмитрий
 */

@Repository("employeeDao")
public class EmployeeDaoImpl extends AbstractDao<Integer, Employee> implements EmployeeDao {
    
    @Override
    public Employee findById(int id) {
        Employee employee = getByKey(id);
        return employee;
    }
 
    @SuppressWarnings("unchecked")
    @Override
    public List<Employee> findAllEmployees() {
        List<Employee> employees = getEntityManager()
                .createQuery("SELECT u FROM Employee u ORDER BY u.firstName ASC")
                .getResultList();
        return employees;
    }
 
    @Override
    public void save(Employee employee) {
        persist(employee);
    }
 
    /**
     *
     * @param id
     */
    @Override
    public void delete(int id) {
        Employee employee =  (Employee) getEntityManager()
                .createQuery("SELECT u FROM Employee u WHERE u.id LIKE :id")
                .setParameter("id", id)
                .getSingleResult();
        delete(employee);
    }

    
}
