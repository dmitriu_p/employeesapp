/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.informationofemployees.dao;

import com.mycompany.informationofemployees.model.Employee;
import java.util.List;

/**
 *
 * @author Дмитрий
 */


public interface EmployeeDao {
    
    Employee findById(int id);
     
    void save(Employee employee);
     
    void delete(int id);
     
    List<Employee> findAllEmployees();
    
}
