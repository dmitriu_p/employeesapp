/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.informationofemployees.dao;

import com.mycompany.informationofemployees.model.Position;
import java.util.List;

/**
 *
 * @author Дмитрий
 */
public interface PositionDao {
    
    List<Position> findAll();
     
    Position findByType(String name);
     
    Position findById(int id);
    
}
