/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.informationofemployees.dao;

import com.mycompany.informationofemployees.model.Position;
import java.util.List;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Дмитрий
 */

@Repository("positionDao")
public class PositionDaoImpl extends AbstractDao<Integer, Position>implements PositionDao{
    
    @Override
    public Position findById(int id) {
        return getByKey(id);
    }
 
    @Override
    public Position findByType(String name) {
        try{
            Position position = (Position) getEntityManager()
                    .createQuery("SELECT p FROM Position p WHERE p.name LIKE :name")
                    .setParameter("name", name)
                    .getSingleResult();
            return position; 
        }catch(NoResultException ex){
            System.out.println("ex: "+ex);
            return null;
        }
    }
     
    @SuppressWarnings("unchecked")
    @Override
    public List<Position> findAll(){
        List<Position> position = getEntityManager()
                .createQuery("SELECT p FROM Position p  ORDER BY p.name ASC")
                .getResultList();
        return position;
    }
}
