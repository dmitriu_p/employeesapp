/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.informationofemployees.service;

import com.mycompany.informationofemployees.dao.DepartmentDao;
import com.mycompany.informationofemployees.model.Department;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Дмитрий
 */

@Service("departmentService")
@Transactional
public class DepartmentServiceImpl implements DepartmentService {
    
    @Autowired
    DepartmentDao dao;
    
    @Override
    public List< Department> findAll() {
        return dao.findAll();
    }
     
    @Override
    public  Department findById(int id) {
        return dao.findById(id);
    }
 
    @Override
    public  Department findByType(String name){
        return dao.findByType(name);
    }
    
}
