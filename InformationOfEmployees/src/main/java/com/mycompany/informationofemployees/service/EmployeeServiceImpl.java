/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.informationofemployees.service;

import com.mycompany.informationofemployees.dao.DepartmentDao;
import com.mycompany.informationofemployees.dao.EmployeeDao;
import com.mycompany.informationofemployees.dao.PositionDao;
import com.mycompany.informationofemployees.model.Department;
import com.mycompany.informationofemployees.model.Employee;
import com.mycompany.informationofemployees.model.Position;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Дмитрий
 */
@Service("employeeService")
public class EmployeeServiceImpl implements EmployeeService {
   
    @Autowired
    private EmployeeDao dao;
    
    @Autowired
    private DepartmentDao daoDept;
    
    @Autowired
    private PositionDao daoPost;
 
    @Override
    public  Employee findById(int id) {
        return dao.findById(id);
    }
 
    @Override
    public void saveEmployee(Employee employee) {
        dao.save(employee);
    }

    @Override
    public void updateEmployee(Employee employee, Department department, Position position) {
        
        Employee entity = dao.findById((int) employee.getId());
        
        Position post = daoPost.findById((int) position.getId());
        Department dept = daoDept.findById((int) department.getId());
        if(entity!=null){
            entity.setFirstName(employee.getFirstName());
            entity.setLastName(employee.getLastName());
            entity.setDepartment(dept);
            entity.setPosition(post);
            
        }
    }
      
    @Override
    public void deleteEmployee(int id) {
        dao.delete(id);
    }
 
    @Override
    public List<Employee> findAllEmployees() {
        return dao.findAllEmployees();
    }

}
