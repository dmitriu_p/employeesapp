/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.informationofemployees.service;

import com.mycompany.informationofemployees.model.Department;
import java.util.List;

/**
 *
 * @author Дмитрий
 */
public interface DepartmentService {
    
    List<Department> findAll();
     
    Department findByType(String name);
     
    Department findById(int id);
    
}
