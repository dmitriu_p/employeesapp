/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.informationofemployees.service;

import com.mycompany.informationofemployees.model.Department;
import com.mycompany.informationofemployees.model.Employee;
import com.mycompany.informationofemployees.model.Position;
import java.util.List;

/**
 *
 * @author Дмитрий
 */
public interface EmployeeService {
    
    Employee findById(int id);
     
    void saveEmployee(Employee employee);
     
    void updateEmployee(Employee employee, Department department, Position position);
     
    void deleteEmployee(int id);
 
    List<Employee> findAllEmployees();
    
}
