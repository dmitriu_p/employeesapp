/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.informationofemployees.service;

import com.mycompany.informationofemployees.dao.PositionDao;
import com.mycompany.informationofemployees.model.Position;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Дмитрий
 */

@Service("positionService")
@Transactional
public class PositionServiceImpl implements PositionService {
    
    @Autowired
    PositionDao dao;
    
    @Override
    public List< Position> findAll() {
        return dao.findAll();
    }
     
    @Override
    public  Position findById(int id) {
        return dao.findById(id);
    }
 
    @Override
    public  Position findByType(String name){
        return dao.findByType(name);
    }
    
}
