/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.testinformationofemployees.dao;

import com.mycompany.informationofemployees.dao.EmployeeDao;
import com.mycompany.informationofemployees.dao.PositionDao;
import com.mycompany.informationofemployees.model.Department;
import com.mycompany.informationofemployees.model.Employee;
import com.mycompany.informationofemployees.model.Position;
import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 *
 * @author Дмитрий
 */
public class EmployeeDaoImplTest extends EntityDaoImplTest {
    
    @Autowired
    EmployeeDao employeeDao;
    
    @Autowired
    PositionDao positionDao;
 
    /*@Override
    protected IDataSet getDataSet() throws Exception{
    IDataSet dataSet = new FlatXmlDataSet(this.getClass().getClassLoader().getResourceAsStream("Employee.xml"));
    return dataSet;
    }*/
    
    @Override
    protected IDataSet getDataSet() throws Exception {
        IDataSet[] datasets = new IDataSet[] {
            new FlatXmlDataSet(this.getClass().getClassLoader().getResourceAsStream("Position.xml")),
            new FlatXmlDataSet(this.getClass().getClassLoader().getResourceAsStream("Department.xml")),
            new FlatXmlDataSet(this.getClass().getClassLoader().getResourceAsStream("Employee.xml"))   
        };
        return new CompositeDataSet(datasets);
    }
 
    @Test
    public void findById(){
        int testId = 1;
        int testIdFalse = 3;
        Assert.assertNotNull(employeeDao.findById(testId));
        Assert.assertNull(employeeDao.findById(testIdFalse));
    }
    
    @Test
    public void findAllEmployees(){
        int testSize = 2;
        Assert.assertEquals(employeeDao.findAllEmployees().size(), testSize);
    }
    
    @Test
    public void saveEmployee(){
        employeeDao.save(getSampleEmployee());
        Assert.assertEquals(employeeDao.findAllEmployees().size(), 3);
    }
    
    @Test
    public void delete(){
        int testId = 1;
        employeeDao.delete(testId);
        Assert.assertEquals(employeeDao.findAllEmployees().size(),  1);
    }
    
     @Test
    public void findByIdPost(){
        int testId = 1;
        int testIdFalse = 3;
        Assert.assertNotNull(positionDao.findById(testId));
        Assert.assertNull(positionDao.findById(testIdFalse));
    }
    
    @Test
    public void findByTypePost(){
        String testName = "Junior";
        String testNameFalse = "Master";
        Assert.assertNotNull(positionDao.findByType(testName));
        Assert.assertNull(positionDao.findByType(testNameFalse));
    }
    
    @Test
    public void findAllPositions(){
        int testSize = 2;
        Assert.assertEquals(positionDao.findAll().size(), testSize);
    }
    
    public Employee getSampleEmployee(){
        Employee employee = new Employee();
        Department dept = new Department(1, "Development");
        Position post = new Position(1, "Junior");        
        employee.setFirstName("Mark");
        employee.setLastName("Happy");
        employee.setDepartment(dept);
        employee.setPosition(post);
        return employee;
    }
    
    
    
}
