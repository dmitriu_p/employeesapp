/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.testinformationofemployees.dao;

import com.mycompany.informationofemployees.dao.DepartmentDao;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 *
 * @author Дмитрий
 */
public class DepartmentDaoImplTest extends EntityDaoImplTest {
    
    @Autowired
    DepartmentDao departmentDao;
 
    @Override
    protected IDataSet getDataSet() throws Exception{
        IDataSet dataSet = new FlatXmlDataSet(this.getClass().getClassLoader().getResourceAsStream("Department.xml"));
        return dataSet;   
    }
    
    @Test
    public void findById(){
        int testId = 1;
        int testIdFalse = 3;
        Assert.assertNotNull(departmentDao.findById(testId));
        Assert.assertNull(departmentDao.findById(testIdFalse));
    }
    
    @Test
    public void findByType(){
        String testName = "Development";
        String testNameFalse = "Charge";
        Assert.assertNotNull(departmentDao.findByType(testName));
        Assert.assertNull(departmentDao.findByType(testNameFalse));
    }
    
    @Test
    public void findAllDepartments(){
        int testSize = 2;
        Assert.assertEquals(departmentDao.findAll().size(), testSize);
    }
       
}
