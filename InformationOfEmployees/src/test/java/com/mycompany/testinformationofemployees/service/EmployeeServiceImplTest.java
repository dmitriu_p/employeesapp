/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.testinformationofemployees.service;

import com.mycompany.informationofemployees.dao.DepartmentDao;
import com.mycompany.informationofemployees.dao.EmployeeDao;
import com.mycompany.informationofemployees.dao.PositionDao;
import com.mycompany.informationofemployees.model.Department;
import com.mycompany.informationofemployees.model.Employee;
import com.mycompany.informationofemployees.model.Position;
import com.mycompany.informationofemployees.service.EmployeeServiceImpl;
import java.util.ArrayList;
import java.util.List;
import org.mockito.InjectMocks;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import org.mockito.Mock;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 *
 * @author Дмитрий
 */
public class EmployeeServiceImplTest {
    
    @Mock
    EmployeeDao dao;
    
    @Mock
    DepartmentDao daoDept;
    
    @Mock
    PositionDao daoPost;
     
    @InjectMocks
    EmployeeServiceImpl employeeService;
     
    @Spy
    List<Employee> employees = new ArrayList<>();
     
    @BeforeClass
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        employees = getEmployeeList();
    }
 
    @Test
    public void findById(){
        Employee emp = employees.get(0);
        when(dao.findById(anyInt())).thenReturn(emp);
        Assert.assertEquals(employeeService.findById((int) emp.getId()),emp);
    }
    
    @Test
    public void saveEmployee(){
        doNothing().when(dao).save(any(Employee.class));
        employeeService.saveEmployee(any(Employee.class));
        verify(dao, atLeastOnce()).save(any(Employee.class));
    }
    
    @Test
    public void updateEmployee(){
        Employee emp = employees.get(0);       
        Department dept = emp.getDepartment();
        System.out.println(dept.getId());
        Position post = emp.getPosition();
        when(dao.findById(anyInt())).thenReturn(emp);
        when(daoDept.findById(anyInt())).thenReturn(dept);
        when(daoPost.findById(anyInt())).thenReturn(post);
        employeeService.updateEmployee(emp, dept, post);
        verify(dao, atLeastOnce()).findById(anyInt());
    }
    
    @Test
    public void deleteEmployeeById(){
        doNothing().when(dao).delete(anyInt());
        employeeService.deleteEmployee(anyInt());
        verify(dao, atLeastOnce()).delete(anyInt());
    }
     
    @Test
    public void findAllEmployees(){
        when(dao.findAllEmployees()).thenReturn(employees);
        Assert.assertEquals(employeeService.findAllEmployees(), employees);
    }
    
    public List<Employee> getEmployeeList(){
        Department dept1 = new Department(1, "Development");
        Position post1 = new Position(1, "Junior");
        Employee e1 = new Employee();
        e1.setId(1);
        e1.setFirstName("Axel");
        e1.setLastName("Royz");
        e1.setDepartment(dept1);
        e1.setPosition(post1);
         
        Employee e2 = new Employee();
        e2.setId(1);
        e2.setFirstName("Axel");
        e2.setLastName("Royz");
        e2.setDepartment(dept1);
        e2.setPosition(post1);
         
        employees.add(e1);
        employees.add(e2);
        return employees;
    }
    
}
