/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.removecolumnofmatrix.main;

import com.mycompany.removecolumnofmatrix.logic.FilterMatrix;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author Дмитрий
 */
public class MainApp {
    
    private static int M, N;
    private static int[][] matrix;
    private static int minLimit = 10;
    private static int maxLimit = 1000;
    
    public static void main(String[] args) {
        
        if(args.length > 0) {
            M = Integer.parseInt(args[0]);
            N = Integer.parseInt(args[1]);
        } else {           
            Scanner in = new Scanner(System.in);
            System.out.print("Enter M: ");
            M = in.nextInt();
            System.out.print("Enter N: ");
            N = in.nextInt();
        }
        fillMatrix(minLimit, maxLimit);
        System.out.println("Original matrix:");
        int[][] matrix1 = {
                {1, 2, 0, 2},
                {1, 0, 3, 0},
        };
        printMatrix(matrix1);
        int[][] m2;
        FilterMatrix mat = new FilterMatrix();
        int column = mat.findMinValue(matrix1);
        m2 = mat.deleteColumn(column, matrix1);
        System.out.println("Result:");
        printMatrix(m2);
        
        
    }
    
    private static void fillMatrix(int minLimit, int maxLimit) {
        matrix = new int[M][N];
        int n = (int)Math.round(minLimit + Math.random() * maxLimit);
        for (int[] matrix1 : matrix) {
            for (int j = 0; j < matrix1.length; j++) {
                matrix1[j] = new Random().nextInt(3);
            }
        }
    }

    private static void printMatrix(int[][] matrix) {
        for (int[] matrix1 : matrix) {
            for (int j = 0; j < matrix1.length; j++) {
                System.out.print(matrix1[j] + "\t");
            }
            System.out.println();
        }
    }
    
}
