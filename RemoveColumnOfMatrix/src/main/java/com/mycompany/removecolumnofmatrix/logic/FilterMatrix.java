/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.removecolumnofmatrix.logic;

import com.mycompany.removecolumnofmatrix.main.MainApp;

/**
 *
 * @author Дмитрий
 */
public class FilterMatrix {
    
    public int[][] deleteColumn(int min, int[][] matrix) {
        int [][] correctMatrix = null;
        int[][] to;
        for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				if (matrix[i][j] == min) {
                   correctMatrix = delete(j, matrix);
                }
            }
        }
        to = correctMatrix;
        return to;
	}

    private int[][] delete(int column, int[][] matrix) {
        int[][] tes;
        int[][] newMatrix = new int[matrix.length][matrix[0].length - 1];
        for (int i = 0; i < matrix.length; i++) {
			int columnCount = 0;
			for (int j = 0; j < matrix[i].length; j++) {
				if(column == j) {
                        continue;
                }
                newMatrix[i][columnCount++] = matrix[i][j];
            }
        }
        System.out.println("NewMatrix:");
                for (int i = 0; i < newMatrix.length; i++) {
			for (int j = 0; j < newMatrix[i].length; j++) {
				System.out.print(newMatrix[i][j] + "\t");
			}
			System.out.println();
		}
        tes = newMatrix;
        return tes;
    }

    public int findMinValue(int[][] matrix) {
        int minM, minN;
        int min = Integer.MAX_VALUE;
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (matrix[i][j] < min) {
                        minM = i;
                        minN = j;
                        min = matrix[i][j];
                        System.out.println("Минимальный: " + min + " Элемент: " + minM + "," + minN);
                }
            }
        }
        return min;
    }
    
}
